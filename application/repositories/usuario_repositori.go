package repositories

import (
	"log"
	"prometheus/domain"

	"gorm.io/gorm"
)

type UsuarioRepositorio interface {
	Insert(usuario *domain.Usuario) (*domain.Usuario, error)
}

type UsuarioRepositorioDB struct {
	Db *gorm.DB
}

func (repo UsuarioRepositorioDB) Insert(usuario *domain.Usuario) (*domain.Usuario, error) {
	err := usuario.Preparar()

	if err != nil {
		log.Fatalf("erro ao validar usuario: %v", err)
		return usuario, err
	}

	err = repo.Db.Create(usuario).Error

	if err != nil {
		log.Fatalf("Criar usuario: %v", err)
		return usuario, err
	}

	return usuario, nil
}
