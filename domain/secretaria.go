package domain

type Secretaria struct {
	ID           uint64 `gorm:"primaryKey"`
	IdPrefeitura uint64
	nome         string
}
