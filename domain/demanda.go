package domain

import (
	"gorm.io/gorm"
)

/*  id Setor e para direcionar o chamado para o sertor correto na hora q usuario criar o chamando

 */
type Demanda struct {
	gorm.Model
	ID           uint64 `gorm:"primaryKey"`
	IdSecretaria uint64
	IdSetor      uint64
}
