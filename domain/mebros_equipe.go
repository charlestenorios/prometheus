package domain

import "gorm.io/gorm"

type MebrosEquipe struct {
	gorm.Model
	IdEquipe    uint64
	Nome        string
	Tercerizado bool
	Funcao      string
	Disponivel  bool
}
