package domain

import (
	"log"

	uuid "github.com/satori/go.uuid"
	"gorm.io/gorm"

	"golang.org/x/crypto/bcrypt"
)

type Usuario struct {
	gorm.Model
	ID    uint64 `gorm:"primaryKey"`
	Nome  string
	Email string `gorm:"index:idx_email,unique"`
	Senha string
	Token string `gorm:"index:idx_token,unique"`
}

func NovoUsuario() *Usuario {
	return &Usuario{}
}

func (usuario *Usuario) Preparar() error {
	senha, err := bcrypt.GenerateFromPassword([]byte(usuario.Senha), bcrypt.DefaultCost)

	if err != nil {
		log.Fatalf("erro ao gera hashe de senha : %v", err)
		return err
	}
	usuario.Senha = string(senha)
	usuario.Token = uuid.NewV4().String()

	err = usuario.validar()

	if err != nil {
		log.Fatalf("Erro de validacao : %v", err)
		return err
	}
	return nil
}

func (usuario *Usuario) validar() error {
	return nil

}
