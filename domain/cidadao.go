package domain

import "gorm.io/gorm"

type Cidadao struct {
	gorm.Model
	ID uint64 `gorm:"primaryKey"`
	Entidade
	Fone         string
	IdPrefeitura uint64
	prefetura    Prefeitura
}
