package domain

import (
	_ "github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type Entidade struct {
	gorm.Model
	ID            uint64 `gorm:"primaryKey"`
	Nome          string
	TipoDocumento string
	Documento     string `gorm:"index:idx_cpf_cnpj,unique"`
	Cep           string
	Cidade        string
	Endereco      string
	Bairro        string
	IdUsuario     uint64
	usuario       Usuario
}

///scope.Statement.SetColumn("ID", uuid.NewV4().String())
