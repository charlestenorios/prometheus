package domain

import (
	"github.com/qor/media/oss"
	"gorm.io/gorm"
)

type Chamado struct {
	gorm.Model
	ID            string
	IdCidadao     uint64
	IdDemanda     uint64
	StatusChamado string
	IdBairro      uint64
	Rua           string
	Descricao     string
	Imagem        oss.OSS
}
