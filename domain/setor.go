package domain

import "gorm.io/gorm"

type Setor struct {
	gorm.Model
	ID           uint64
	IdSecretaria uint64
	nome         string
}
