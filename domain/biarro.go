package domain

import "gorm.io/gorm"

type Bairro struct {
	gorm.Model
	ID       uint64
	IdCidade uint64
	nome     string
}
