package domain

import "gorm.io/gorm"

type Equipe struct {
	gorm.Model
	ID           uint64
	IdSecretaria uint64
	Nome         string
	QtdMembros   uint
	OsAtual      string
}
