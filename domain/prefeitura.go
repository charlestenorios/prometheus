package domain

import "gorm.io/gorm"

type Prefeitura struct {
	gorm.Model
	ID        uint64 `gorm:"primaryKey"`
	Cnpj      string `gorm:"index:idx_cnpj,unique"`
	Nome      string
	IdUsuario uint64
	usuario   Usuario
}
