package domain

import (
	"gorm.io/gorm"
)

type OsGerada struct {
	gorm.Model
	ID                string
	IdChamado         uint64
	IdSecretaria      uint64
	IdDemandaa        uint64
	DescricaoProblema string
	StatusChamado     string
}
