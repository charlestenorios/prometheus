package utils

import (
	"log"

	"prometheus/domain"

	_ "github.com/joho/godotenv"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func ConectarDB() *gorm.DB {

	dns := "host=localhost user=postgres password=linux123 dbname=prefeitura port=5432 sslmode=disable"
	db, err := gorm.Open(postgres.Open(dns), &gorm.Config{})
	if err != nil {
		log.Fatalf("erro ao connectar no banco: %v", err)
		panic(err)
	}

	//defer db.Close()
	db.AutoMigrate(&domain.Usuario{},
		&domain.Entidade{},
		&domain.Prefeitura{},
		&domain.Cidadao{})

	return db

}
