module prometheus

go 1.15

require (
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/qor/admin v0.0.0-20210305071740-2c1a65734f67 // indirect
	github.com/qor/media v0.0.0-20201118025607-96c1f2487abe // indirect
	github.com/qor/qor v0.0.0-20200729071734-d587cffbbb93 // indirect
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	gorm.io/driver/postgres v1.0.8
	gorm.io/gorm v1.21.3
)
